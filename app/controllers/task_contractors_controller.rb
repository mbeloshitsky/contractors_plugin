class TaskContractorsController < ApplicationController

  menu_item :task_contractors

  before_action :authorize_global
  before_action :find_task_contractor, only: [:show, :edit, :update]
  before_action :find_task_contractors, only: [:context_menu, :bulk_edit, :bulk_update, :destroy]

  helper :task_contractors
  helper :custom_fields, :context_menus, :attachments, :issues
  include_query_helpers

  accept_api_auth :index, :show, :create, :update, :destroy

  def index
    retrieve_query(TaskContractorQuery)
    @entity_count = @query.task_contractors.count
    @entity_pages = Paginator.new @entity_count, per_page_option, params['page']
    @entities = @query.task_contractors(offset: @entity_pages.offset, limit: @entity_pages.per_page)
  end

  def show
    respond_to do |format|
      format.html
      format.api
      format.js
    end
  end

  def new
    @task_contractor = TaskContractor.new
    @task_contractor.safe_attributes = params[:task_contractor]

    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @task_contractor = TaskContractor.new author: User.current
    @task_contractor.safe_attributes = params[:task_contractor]
    @task_contractor.save_attachments(params[:attachments] || (params[:task_contractor] && params[:task_contractor][:uploads]))

    respond_to do |format|
      if @task_contractor.save
        format.html do
          flash[:notice] = l(:notice_successful_create)
          redirect_back_or_default task_contractor_path(@task_contractor)
        end
        format.api { render action: 'show', status: :created, location: task_contractor_url(@task_contractor) }
        format.js { render template: 'common/close_modal' }
      else
        format.html { render action: 'new' }
        format.api { render_validation_errors(@task_contractor) }
        format.js { render action: 'new' }
      end
    end
  end

  def create_ajax
    @task_contractor = TaskContractor.find_by_name(params[:task_contractor][:name])
    if @task_contractor
      render json: @task_contractor
      return
    end
    @task_contractor = TaskContractor.new author: User.current
    @task_contractor.safe_attributes = params[:task_contractor]
    @task_contractor.save_attachments(params[:attachments] || (params[:task_contractor] && params[:task_contractor][:uploads]))

    if @task_contractor.save
      render json: @task_contractor
    else
      render_validation_errors(@task_contractor)
    end
  end


  def edit
    @task_contractor.safe_attributes = params[:task_contractor]

    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    @task_contractor.safe_attributes = params[:task_contractor]
    @task_contractor.save_attachments(params[:attachments] || (params[:task_contractor] && params[:task_contractor][:uploads]))

    respond_to do |format|
      if @task_contractor.save
        format.html do
          flash[:notice] = l(:notice_successful_update)
          redirect_back_or_default task_contractor_path(@task_contractor)
        end
        format.api { render_api_ok }
        format.js { render template: 'common/close_modal' }
      else
        format.html { render action: 'edit' }
        format.api { render_validation_errors(@task_contractor) }
        format.js { render action: 'edit' }
      end
    end
  end

  def destroy
    @task_contractors.each(&:destroy)

    respond_to do |format|
      format.html do
        flash[:notice] = l(:notice_successful_delete)
        redirect_back_or_default task_contractors_path
      end
      format.api { render_api_ok }
    end
  end

  def bulk_edit
  end

  def bulk_update
    unsaved, saved = [], []
    attributes = parse_params_for_bulk_update(params[:task_contractor])
    @task_contractors.each do |entity|
      entity.init_journal(User.current) if entity.respond_to? :init_journal
      entity.safe_attributes = attributes
      if entity.save
        saved << entity
      else
        unsaved << entity
      end
    end
    respond_to do |format|
      format.html do
        if unsaved.blank?
          flash[:notice] = l(:notice_successful_update)
        else
          flash[:error] = unsaved.map{|i| i.errors.full_messages}.flatten.uniq.join(",\n")
        end
        redirect_back_or_default :index
      end
    end
  end

  def context_menu
    if @task_contractors.size == 1
      @task_contractor = @task_contractors.first
    end

    can_edit = @task_contractors.detect{|c| !c.editable?}.nil?
    can_delete = @task_contractors.detect{|c| !c.deletable?}.nil?
    @can = {edit: can_edit, delete: can_delete}
    @back = back_url

    @task_contractor_ids, @safe_attributes, @selected = [], [], {}
    @task_contractors.each do |e|
      @task_contractor_ids << e.id
      @safe_attributes.concat e.safe_attribute_names
      attributes = e.safe_attribute_names - (%w(custom_field_values custom_fields))
      attributes.each do |c|
        column_name = c.to_sym
        if @selected.key? column_name
          @selected[column_name] = nil if @selected[column_name] != e.send(column_name)
        else
          @selected[column_name] = e.send(column_name)
        end
      end
    end

    @safe_attributes.uniq!

    render layout: false
  end

  def autocomplete
  end

  private

  def find_task_contractor
    @task_contractor = TaskContractor.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end

  def find_task_contractors
    @task_contractors = TaskContractor.visible.where(id: (params[:id] || params[:ids])).to_a
    @task_contractor = @task_contractors.first if @task_contractors.count == 1
    raise ActiveRecord::RecordNotFound if @task_contractors.empty?
    raise Unauthorized unless @task_contractors.all?(&:visible?)

    @projects = @task_contractors.collect(&:project).compact.uniq
    @project = @projects.first if @projects.size == 1
  rescue ActiveRecord::RecordNotFound
    render_404
  end


end
