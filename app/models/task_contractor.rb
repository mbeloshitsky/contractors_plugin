class TaskContractor < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include Redmine::SafeAttributes

  belongs_to :author, class_name: 'User'

  scope :visible, ->(*args) { where(TaskContractor.visible_condition(args.shift || User.current, *args)) }  
  scope :sorted, -> { order("#{table_name}.name ASC") }

  

  # Contractors are not scoped to projects, so we set project_key: '1'
  # to remove error while per project search
  acts_as_searchable columns: ["#{TaskContractor.table_name}.name"],
                     project_key: '1',
                     date_column: :created_at
  acts_as_customizable
  acts_as_attachable
  acts_as_activity_provider author_key: :author_id, timestamp: "#{table_name}.created_at"

  
  validates :author_id, presence: true
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  safe_attributes *%w[name custom_field_values custom_fields]

  after_create :send_create_notification
  after_update :send_update_notification

  def self.visible_condition(user, options = {})
    '1=1'
  end

  def self.css_icon
    'icon icon-user'
  end

  def editable_by?(user)
    editable?(user)
  end

  def project
    nil
  end

  def event_type
    'user'
  end

  def event_title
    name
  end

  def event_url
    task_contractor_path(self)
  end

  def description
    result = []
    custom_values.each do |cv|
      result.push("#{cv.custom_field.name}: #{cv.value}")
    end
    result.join(', ')
  end

  def event_description
    description
  end

  def event_datetime
    created_at
  end

  def visible?(user = nil)
    user ||= User.current
    user.allowed_to?(:view_task_contractors, project, global: true)
  end

  def editable?(user = nil)
    user ||= User.current
    user.allowed_to?(:manage_task_contractors, project, global: true)
  end

  def deletable?(user = nil)
    user ||= User.current
    user.allowed_to?(:manage_task_contractors, project, global: true)
  end

  def attachments_visible?(user = nil)
    visible?(user)
  end

  def attachments_editable?(user = nil)
    editable?(user)
  end

  def attachments_deletable?(user = nil)
    deletable?(user)
  end

  def to_s
    name
  end

  alias_attribute :created_on, :created_at
  alias_attribute :updated_on, :updated_at

  def notified_users
    if project
      project.notified_users.reject {|user| !visible?(user)}
    else
      [User.current]
    end
  end

  def send_create_notification
    # if Setting.notified_events.include?('task_contractor_added')
    notified_users.each do |user|
      TaskContractorMailer.task_contractor_added(user, self).deliver
    end
    # end
  end

  def send_update_notification
    # if Setting.notified_events.include?('task_contractor_updated')
    notified_users.each do |user|
      TaskContractorMailer.task_contractor_updated(user, self).deliver
    end
    # end
  end

end
