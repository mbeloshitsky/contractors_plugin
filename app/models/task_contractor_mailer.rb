class TaskContractorMailer < Mailer

  helper :task_contractors

  def task_contractor_added(recipient, task_contractor)
    @author = task_contractor.author
    @task_contractor = task_contractor
    @task_contractor_url = url_for(controller: 'task_contractors', action: 'show', id: task_contractor)

    message_id task_contractor
    references task_contractor

    mail to: recipient,
      subject: "#{l(:label_task_contractor)}: #{task_contractor.to_s}"
  end

  def task_contractor_updated(recipient, task_contractor)
    @author = task_contractor.author
    @task_contractor = task_contractor
    @task_contractor_url = url_for(controller: 'task_contractors', action: 'show', id: task_contractor)

    message_id task_contractor
    references task_contractor

    mail to: recipient,
      subject: "#{l(:label_task_contractor)}: #{task_contractor.to_s}"
  end

end
