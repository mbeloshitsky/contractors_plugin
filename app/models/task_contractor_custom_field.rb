class TaskContractorCustomField < CustomField

  def type_name
    :label_task_contractors
  end

  def form_fields
    [:is_filter, :searchable, :is_required]
  end

end
