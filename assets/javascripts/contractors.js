;(function () {
    var handlers = {};

    window.searchBoxResolve = null;

    window.setupSearchField = function (id, value, contractors) {
        var button = document.querySelector('#' + id + '_button');
    
        if (handlers[id]) {
            handlers[id].destroy();
        }
        handlers[id] = new SlimSelect({
            data: [null].concat(contractors).map(function (contractor) {
                if (!contractor) {
                    return {
                        text: '-',
                        value: 0,
                        data: {
                            search: 'Не выбран'
                        }
                    }
                }
                return {
                    text: contractor.name,
                    value: contractor.id,
                    selected: contractor.id === Number(value || 0),
                    data: {
                        search: (contractor.name + ' ' + contractor.description).toLowerCase()
                    }
                }
            }),
            select: '#' + id,
            placeholder: 'Контрагент не выбран',
            searchText: 'Контрагент не найден',
            searchPlaceholder: 'Поиск...',
            searchFilter: (option, search) => {
                return option.data.search.indexOf(search.toLowerCase()) !== -1
            },
        });
        var select = document.querySelector('#' + id);
        var nullOption = select.firstChild;
        nullOption.value = '';

        button.addEventListener('mousedown', function () {
            new Promise(function (resolve, reject) {
                window.searchBoxResolve = resolve;
            })
            .then(function (data) {
                var select = handlers[id];
                select.addData({ value: data.id, text: data.name });
                select.setSelected(data.id);
            })
        })
    }
    
})();
