Redmine::Plugin.register :contractors do
  name 'Contractors'
  author ''
  author_url ''
  description ''
  version '2020'

  #into easy_settings goes available setting as a symbol key, default value as a value
  settings easy_settings: {  }
  
end


unless Redmine::Plugin.registered_plugins[:easy_extensions]
  require_relative 'after_init'
end

Rails.configuration.to_prepare do
  Redmine::FieldFormat.module_eval do 
    class ContractorFormat < Redmine::FieldFormat::RecordList
      add 'contractor'
      self.multiple_supported = false
      self.form_partial = 'task_contractors/custom_field'

      def target_class
        TaskContractor
      end

      def edit_tag(view, tag_id, tag_name, custom_value, options={})
        listId = tag_id + '_list'
        searchId = tag_id + '_search'

        contractor = TaskContractor.find_by_id(custom_value.value)
        s = ''.html_safe
        s += select_edit_tag(view, tag_id, tag_name, custom_value, options)
        s += ' '
        s += view.link_to(
          l(:add_contractor_btn),
          view.new_task_contractor_path,
          :class => 'icon icon-add contractor_new_button',
          :id => tag_id + '_button',
          :remote => true,
          :method => 'get'
        )
        s += view.content_tag('script', "setupSearchField('#{tag_id}', '#{custom_value.value}', #{TaskContractor.all.to_json(only: [:id, :name], methods: :description)});".html_safe);

        view.content_tag('span', s, { :class => 'contractor_wrap' })
      end
     
      def possible_values_options(custom_field, object=nil)
        contractors = possible_values_records(custom_field, object)
        contractors.map {|u| [u.name, u.id.to_s]}
      end

      def possible_values_records(custom_field, object=nil)
        TaskContractor.all
      end

      def value_from_keyword(custom_field, keyword, object)
        users = possible_values_records(custom_field, object).to_a
        parse_keyword(custom_field, keyword) do |k|
          Principal.detect_by_keyword(users, k).try(:id)
        end
      end

      def query_filter_values(custom_field, query)
        possible_values_options(custom_field)
      end

      def formatted_value(view, custom_field, value, customized=nil, html=false)
        if !value
          return '-'
        end
        contractor = TaskContractor.find_by_id(value)
        if !contractor
          return '-'
        end
        if html
          view.link_to contractor.name, view.task_contractor_path(contractor)
        else 
          contractor.name
        end
        # casted = cast_value(custom_field, value, customized)
        # if html && custom_field.url_pattern.present?
        #   texts_and_urls = Array.wrap(casted).map do |single_value|
        #     text = view.format_object(single_value, false).to_s
        #     url = url_from_pattern(custom_field, single_value, customized)
        #     [text, url]
        #   end
        #   links = texts_and_urls.sort_by(&:first).map do |text, url|
        #     css_class = (/^https?:\/\//.match?(url)) ? 'external' : nil
        #     view.link_to_if uri_with_safe_scheme?(url), text, url, :class => css_class
        #   end
        #   links.join(', ').html_safe
        # else
        #   casted
        # end
      end
    end

  end
end


