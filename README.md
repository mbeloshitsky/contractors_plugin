# Setup

Copy plugin into `redmine/plugins` folder. Run command:

```
bundle exec rake redmine:plugins:migrate
```

and restart redmine server.