
  resources :task_contractors do
    collection do 
      post 'create_ajax'
      get 'autocomplete'
      get 'bulk_edit'
      post 'bulk_update'
      get 'context_menu'
    end
  end
