Dir[File.dirname(__FILE__) + '/lib/contractors/easy_patch/**/*.rb'].each {|file| require_dependency file }
Dir[File.dirname(__FILE__) + '/extra/easy_patch/**/*.rb'].each { |file| require_dependency file }

# this block is executed once just after Redmine is started
# means after all plugins are initialized
# it is place for plain requires, not require_dependency
# it should contain hooks, permissions - base class in Redmine is required thus is not reloaded
ActiveSupport.on_load(:easyproject, yield: true) do
  require 'contractors/internals'
  require 'contractors/hooks'
end

# this block is called every time rails are reloading code
# in development it means after each change in observed file
# in production it means once just after server has started
# in this block should be used require_dependency, but only if necessary.
# better is to place a class in file named by rails naming convency and let it be loaded automatically
# Here goes query registering, custom fields registering and so on
RedmineExtensions::Reloader.to_prepare do

end

ActiveSupport.on_load(:easyproject, yield: true) do
  require 'contractors/task_contractor_hooks'

  Redmine::AccessControl.map do |map|
    map.project_module :task_contractors do |pmap|
      pmap.permission :view_task_contractors, { task_contractors: [:index, :show, :autocomplete, :context_menu] }, read: true
      pmap.permission :manage_task_contractors, { task_contractors: [:new, :create, :edit, :update, :destroy, :bulk_edit, :bulk_update] }
    end 
  end

  Redmine::MenuManager.map :top_menu do |menu|
    menu.push :task_contractors, { controller: 'task_contractors', action: 'index', project_id: nil }, caption: :label_task_contractors
  end

  CustomFieldsHelper::CUSTOM_FIELDS_TABS << {name: 'TaskContractorCustomField', partial: 'custom_fields/index', label: :label_task_contractors}

  Redmine::Search.map do |search|
    search.register :task_contractors
  end

  Redmine::Activity.map do |activity|
    activity.register :task_contractors, {class_name: %w(TaskContractor), default: false}
  end

end