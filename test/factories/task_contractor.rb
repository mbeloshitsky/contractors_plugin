FactoryBot.define do

  factory :task_contractor do
    sequence(:name) { |n| "name-#{n}"}
    association :author, factory: :user
    
  end

end
