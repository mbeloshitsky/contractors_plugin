namespace :redmine do
    desc 'Contractors migration script'
    task :migrate_contractors, [:cf_from_id, :cf_to_id] => :environment do |t, args|
        id_from = args[:cf_from_id]
        cf_from = CustomField.find_by_id(id_from)
        id_to = args[:cf_to_id]
        cf_to = CustomField.find_by_id(id_to)
        cf_to.trackers = cf_from.trackers
        if cf_from.is_for_all 
            cf_to.is_for_all = cf_from.is_for_all
            cf_to.save
        else
            cf_to.projects = cf_from.projects
        end
        possible_values = cf_from.possible_values
        possible_values.each do |pv|
            TaskContractor.create(:name => pv, :author => User.find_by_id(4))
        end
        custom_values = CustomValue.where(:custom_field_id => id_from)
        custom_values.each do |cv|
            cv.custom_field_id = id_to
            tv = TaskContractor.find_by_name(cv.value)
            cv.value = tv ? tv.id.to_s : ''
            cv.save
        end
    end
end