module Contractors
  module Hooks
    class IncludeJsHook < Redmine::Hook::ViewListener      
      include ActionView::Helpers::TagHelper

      def view_layouts_base_html_head(context)
        [
          stylesheet_link_tag('slimselect.min', :plugin => 'contractors'),
          javascript_include_tag('slimselect.min', :plugin => 'contractors'),
          stylesheet_link_tag('contractors', :plugin => 'contractors'),
          javascript_include_tag('contractors', :plugin => 'contractors'),
        ]
      end
    end
  end
end
