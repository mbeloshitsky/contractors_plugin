class AddUniqueToTaskContractors < ActiveRecord::Migration[5.2]
  def change
    add_index :task_contractors, [:name], :unique => true
  end
end
