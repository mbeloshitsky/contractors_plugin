class CreateTaskContractors < ActiveRecord::Migration[5.2]
  def change
    create_table :task_contractors do |t|
      t.string :name, null: true
      t.integer :author_id, null: false
      t.belongs_to :author      

      t.timestamps
    end
  end
end
